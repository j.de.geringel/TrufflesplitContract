var addressA;
var addressB;

var meta = EtherSplitter.deployed();


function setStatus(message) {
  var status = document.getElementById("status");
  status.innerHTML = message;
};

var events = meta.allEvents();

// watch for events
events.watch(function(error, event){
  if (!error)
    console.log(event);
});

function init() {
  document.getElementById("addressC").innerHTML = meta.address;

  meta.getAccountA.call().then(function(address) {
    var addresA_element = document.getElementById("addressA");
    addresA_element.innerHTML = address;
    addressA = address;

    meta.getAccountB.call().then(function(address) {
      var addresB_element = document.getElementById("addressB");
      addresB_element.innerHTML = address;
      addressB = address;
      updateBalances();
    });

  }).catch(function(e) {
    console.log(e);
    setStatus("Error getting balance; see log.");
  });
};


function sendCoin() {

  var amount = parseInt(document.getElementById("amount").value);
  var sender = document.getElementById("sender").value;

  setStatus("Initiating transaction... (please wait)");
  var wei = web3.toWei(amount, "ether");

  meta.sendCoin({from: sender, value: wei}).then(function(error, result) {
    setStatus("Transaction complete!" + result);
    updateBalances();

  }).catch(function(e) {
    console.log(e);
    setStatus("Error sending coin; see log.");
  });
};

window.onload = function() {
  var myVar = setInterval(unlock, 60000);
  init();
}

// for local use, convience use. geth should be started with personal api
function unlock(){
  // var currentdate = new Date();

  // console.log('unlocking...'+currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds());
  //web3.personal.unlockAccount(web3.eth.accounts[0], "localpw");
}

function updateBalances(){
 web3.eth.getBalance(addressA, function(error, balance) {
  if (balance  === null){
    balance = 0;
  }
  var balanceAElement =  document.getElementById("balanceA");
  balanceAElement.innerHTML = balance;
});

 web3.eth.getBalance(addressB, function(error, balance) {
  if (balance  === null){
    balance = 0;
  }
  var balanceBElement =  document.getElementById("balanceB");
  balanceBElement.innerHTML = balance;
});

 web3.eth.getBalance(meta.address, function(error, balance) {
  if (balance  === null){
    balance = 0;
  }
  var balanceCElement =  document.getElementById("balanceC");
  balanceCElement.innerHTML = balance;
});
}


