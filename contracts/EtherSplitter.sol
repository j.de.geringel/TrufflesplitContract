
/** Simple contract that divides incoming either to 2 fixed benificary accounts */
contract EtherSplitter {

	address owner;
	// first thought was to pass them as construtor params of the contract but truffle doesn't support that in default config, so hard coded for now
	address accountA = 0x0c3572640e481d21374d39fe111556eb1ed93531;
	address accountB = 0x3287338e269e5ef494895e84079777b1eda522c2;
	mapping (address => uint) balances;

	function EtherSplitter() {
		owner = msg.sender;
	}

	// Function to recover the funds on the contract
    function kill() { 
    	if (msg.sender == owner) selfdestruct(owner); 
    }
	
	event EnteredSendCoin(
        uint _value
    );

    event SendCoinSuccess(
        uint _value
    );

     event SendCoinFail(
        uint _value
    );

	function sendCoin() returns(bool status) {
		EnteredSendCoin(msg.value);
		// division truncates, what's fine with me.
		if (accountA.send(msg.value/2) && accountB.send(msg.value/2)){
			SendCoinSuccess(msg.value/2);
			return true;
		}
		else {
			SendCoinFail(msg.value);
			return false;
		}
	}

	function getAccountA() returns(address addr){
		return accountA;
	}

	function getAccountB() returns(address addr){
		return accountB;
	}
}
